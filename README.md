# Wallpapers

![Alt text](image.png)

## m4k Wallpaper Collection

I have collected these wallpapers over a number of years and have no idea where I got most of them.  I have also renamed them all (giving them a number) so I don't even have their original filename.

## Where did I get these?

Some of the wallpapers were probably included in default wallpaper packages from various Linux distributions that I have installed over the years.
I find wallpapers in a number of different locations on the web.

## Style of Wallpapers

The vast majority of these wallpapers are nature and landscape photos.  There are a few abstract art wallpapers mixed in.

## Ownership

Because I downloaded most of these from sites like Imgur and /wg/, I have no way of knowing if there is a copyright on these images. If you find an image hosted in this repository that is yours and of limited use, please let me know and I will remove it.
